package com.ajit.repository;

import com.ajit.model.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String>{

	public Employee findByEmpId(String empId);
}
