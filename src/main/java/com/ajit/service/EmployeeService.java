package com.ajit.service;

import com.ajit.model.Employee;
import com.ajit.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	public Employee save(Employee employee) {
		return employeeRepository.save(employee);
	}
	
	public Employee findByEmpId(String empId) {
		return employeeRepository.findByEmpId(empId);
	}
	
	public List<Employee> findAllEmployee() {
		return employeeRepository.findAll();
	}
	
	public Employee update(Employee emp) {
		Employee employee = employeeRepository.findByEmpId(emp.getEmpId());
		employee.setAge(emp.getAge());
		employee.setDob(emp.getDob());
		employee.setEmpId(emp.getEmpId());
		employee.setName(emp.getName());
		employee.setSalary(emp.getSalary());
		
		employeeRepository.save(employee);
		return employee;
		
	}
}
