package com.ajit.controller;

import com.ajit.EncryDecrypt;
import com.ajit.info.EmployeeResponse;
import com.ajit.model.Employee;
import com.ajit.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping(value="v1/receiver")
public class ReceiverController {

	@Autowired
	EmployeeService employeeService;


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity readValue(@RequestBody Employee employee, HttpServletRequest request) {
		EmployeeResponse resp = new EmployeeResponse<>();
		//String secretKey = request.getHeader("secretKey");
		//Object employee = request.getHeader("employee");
		//String str = EncryDecrypt.decrypt(employee,secretKey);

		//Employee employee1 = new ObjectMapper().readValue(str, Employee.class);

		//ObjectMapper mapper = new ObjectMapper();
		//Employee employee1 = mapper.readValue(str, Employee.class);

		String type = request.getHeader("fileType");
		try {
			if(type.equalsIgnoreCase("csv")) {
				resp.setData(employeeService.save(employee));
			} else if(type.equalsIgnoreCase("xml")) {

			} else {
				throw new Exception("Invalid file type");
			}
			resp.setMessage("Employee record created successfully");
			return ResponseEntity.status(HttpStatus.CREATED).body(resp);
		} catch (Exception e) {
			resp.setData("Invalid data");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp);
		}
	}
	
}
