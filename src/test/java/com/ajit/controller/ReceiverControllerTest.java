package com.ajit.controller;

import com.ajit.info.EmployeeResponse;
import com.ajit.model.Employee;
import com.ajit.service.EmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

@RunWith(MockitoJUnitRunner.class)
public class ReceiverControllerTest {

    @InjectMocks
    ReceiverController receiverController;

    @Mock
    HttpServletRequest request;

    @Mock
    EmployeeResponse employeeResponse;

    @Mock
    ResponseEntity responseEntity;

    @Mock
    EmployeeService employeeService;

    @Test
    public void readValueTest() {
        Employee employee = new Employee();
        request.setAttribute("fileType", "csv");
        Mockito.when(employeeService.save(employee)).thenReturn(employee);
        receiverController.readValue(employee, request);
    }

    @Test
    public void readValueDataTest() {
        Employee employee = new Employee();
        request.setAttribute("fileType", "xml");
        employee.setEmpId("101");

        Mockito.when(employeeService.save(employee)).thenReturn(employee);
        receiverController.readValue(employee, request);
    }
}
